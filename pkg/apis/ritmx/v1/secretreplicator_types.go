/*
Copyright 2019 The secret-replicator Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
)

// SecretReplicatorSpec defines the desired state of secretReplicator
// +k8s:openapi-gen=true
type SecretReplicatorSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html

	// +kubebuilder:validation:Pattern=[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*
	Name string `json:"name"`

	// +kubebuilder:validation:Pattern=[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*
	ReplicantsName string `json:"replicantsName,omitempty"`

	// +kubebuilder:validation:Pattern=[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*
	Namespace string `json:"namespace"`

	NamespaceSelector metav1.LabelSelector `json:"namespaceSelector,omitempty"`
}

// SecretReplicatorStatus defines the observed state of secretReplicator
// +k8s:openapi-gen=true
type SecretReplicatorStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html

	ReplicantsName string `json:"replicantsName"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// SecretReplicator is the Schema for the secretReplicators API
// +k8s:openapi-gen=true
// +genclient:nonNamespaced
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="SecretName",type="string",JSONPath=".spec.name",description="Name of the Secret to replicate"
// +kubebuilder:printcolumn:name="Namespace",type="string",JSONPath=".spec.namespace",description="Namespace of the Secret to replicate"
// +kubebuilder:printcolumn:name="ReplicantsName",type="string",JSONPath=".spec.replicantsName",description="Name of the replicated Secrets"
type SecretReplicator struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   SecretReplicatorSpec   `json:"spec,omitempty"`
	Status SecretReplicatorStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// SecretReplicatorList contains a list of secretReplicator
type SecretReplicatorList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SecretReplicator `json:"items"`
}

func init() {
	SchemeBuilder.Register(&SecretReplicator{}, &SecretReplicatorList{})
}

//GetSourceName return the source secret name
func (r *SecretReplicator) GetSourceName() string {
	return r.Spec.Name
}

//GetSourceNamespace return the source secret namespace
func (r *SecretReplicator) GetSourceNamespace() string {
	return r.Spec.Namespace
}

//GetSourceSecretNamespacedName return a NamespacedName for source secret
func (r *SecretReplicator) GetSourceSecretNamespacedName() types.NamespacedName {
	return types.NamespacedName{Name: r.Spec.Name, Namespace: r.Spec.Namespace}
}

//GetReplicantsName return the target secret name
func (r *SecretReplicator) GetReplicantsName() string {
	if r.Spec.ReplicantsName == "" {
		return r.Spec.Name
	}
	return r.Spec.ReplicantsName
}

//GetStatusReplicantsName return the previous target secret name
func (r *SecretReplicator) GetStatusReplicantsName() string {
	return r.Status.ReplicantsName
}

//SetStatusReplicantsName set the previous target secret name
func (r *SecretReplicator) SetStatusReplicantsName(name string) {
	r.Status.ReplicantsName = name
}

//GetNamespacesSelector return a labels.Selector
func (r *SecretReplicator) GetNamespacesSelector() (labels.Selector, error) {
	//if r.Spec.NamespaceSelector == nil {
	//	return metav1.LabelSelectorAsSelector(&metav1.LabelSelector{})
	//}
	return metav1.LabelSelectorAsSelector(&r.Spec.NamespaceSelector)
}
