/*
Copyright 2019 The secret-replicator Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretreplicator

import (
	"context"
	"os"
	"reflect"
	"regexp"

	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"

	ritmxv1 "ritmx/secret-replicator/pkg/apis/ritmx/v1"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/validation"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const annotationAllowReplicate = "secretreplicators.ritmx.com/allow-replicate"

var log = logf.Log.WithName("controller_secret-replicator")

// Add creates a new secretReplicator Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcilesecretReplicator{
		client:   mgr.GetClient(),
		scheme:   mgr.GetScheme(),
		recorder: mgr.GetEventRecorderFor("secret-replicator-controller"),
		sources:  newSecretWatchFilter(),
		crdKind:  reflect.TypeOf(&ritmxv1.SecretReplicator{}).Elem().Name(),
	}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, rr reconcile.Reconciler) error {
	r := rr.(*ReconcilesecretReplicator)

	// Create a new controller
	c, err := controller.New("secret-replicator-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource secretReplicator
	err = c.Watch(
		&source.Kind{Type: &ritmxv1.SecretReplicator{}},
		&handler.EnqueueRequestForObject{},
		predicate.Funcs{
			CreateFunc: func(e event.CreateEvent) bool { return true },
			UpdateFunc: func(e event.UpdateEvent) bool { return true },
			DeleteFunc: func(e event.DeleteEvent) bool {
				namespacedName := types.NamespacedName{Name: e.Meta.GetName(), Namespace: e.Meta.GetNamespace()}
				r.sources.Delete(namespacedName)
				return true
			},
			GenericFunc: func(e event.GenericEvent) bool { return true },
		})
	if err != nil {
		return err
	}

	// Watch for changes to secondary resource Secret and requeue the owner secretReplicator
	err = c.Watch(
		&source.Kind{Type: &corev1.Secret{}},
		&handler.EnqueueRequestForOwner{
			IsController: true,
			OwnerType:    &ritmxv1.SecretReplicator{},
		}, predicate.Funcs{
			CreateFunc:  func(e event.CreateEvent) bool { return false },
			UpdateFunc:  func(e event.UpdateEvent) bool { return true },
			DeleteFunc:  func(e event.DeleteEvent) bool { return true },
			GenericFunc: func(e event.GenericEvent) bool { return true },
		})
	if err != nil {
		return err
	}

	// Watch for changes to source Secrets
	err = c.Watch(
		&source.Kind{Type: &corev1.Secret{}},
		&handler.EnqueueRequestsFromMapFunc{
			ToRequests: handler.ToRequestsFunc(
				func(a handler.MapObject) []reconcile.Request {
					namespacedName := types.NamespacedName{Name: a.Meta.GetName(), Namespace: a.Meta.GetNamespace()}
					secretReplicatorName, ok := r.sources.Get(namespacedName)
					if !ok {
						return []reconcile.Request{}
					}
					return []reconcile.Request{
						{NamespacedName: types.NamespacedName{
							Name: secretReplicatorName,
						}},
					}
				}),
		},
		predicate.Funcs{
			CreateFunc:  func(e event.CreateEvent) bool { return r.sources.Filter(e.Meta) },
			UpdateFunc:  func(e event.UpdateEvent) bool { return r.sources.Filter(e.MetaOld) },
			DeleteFunc:  func(e event.DeleteEvent) bool { return false },
			GenericFunc: func(e event.GenericEvent) bool { return false },
		})
	if err != nil {
		return err
	}

	// Watch namespace creation
	err = c.Watch(
		&source.Kind{Type: &corev1.Namespace{}},
		&handler.EnqueueRequestsFromMapFunc{
			ToRequests: handler.ToRequestsFunc(
				func(a handler.MapObject) []reconcile.Request {
					return []reconcile.Request{
						{NamespacedName: types.NamespacedName{
							Namespace: a.Meta.GetName(),
						}},
					}
				}),
		},
		predicate.Funcs{
			CreateFunc:  func(e event.CreateEvent) bool { return true },
			UpdateFunc:  func(e event.UpdateEvent) bool { return true },
			DeleteFunc:  func(e event.DeleteEvent) bool { return false },
			GenericFunc: func(e event.GenericEvent) bool { return false },
		})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcilesecretReplicator{}

// ReconcilesecretReplicator reconciles a secretReplicator object
type ReconcilesecretReplicator struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client   client.Client
	scheme   *runtime.Scheme
	recorder record.EventRecorder

	sources *secretWatchFilter
	crdKind string
}

// Reconcile reads that state of the cluster for a secretReplicator object and makes changes based on the state read
// and what is in the secretReplicator.Spec
func (r *ReconcilesecretReplicator) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Name", request.Name, "Request.Namespace", request.Namespace)
	reqLogger.Info("Reconciling SecretReplicator")

	//Get the SecretReplicants
	secretReplicatorList, err := r.listSecretReplicators(request.Name)
	if err != nil {
		reqLogger.Error(err, "Failed to list SecretReplicator.")
		return reconcile.Result{}, err
	}
	if len(secretReplicatorList.Items) == 0 {
		return reconcile.Result{}, nil
	}

	//Get the Namespaces
	namespaceList, err := r.listNamespaces(request.Namespace)
	if err != nil {
		reqLogger.Error(err, "Failed to list namespaces.")
		return reconcile.Result{}, err
	}

	for _, secretReplicator := range secretReplicatorList.Items {

		if !r.validateSecretReplicator(&secretReplicator) {
			reqLogger.Info("SecretReplicator Spec is invalid",
				"Name", secretReplicator.GetName(), "Namespace", secretReplicator.GetSourceNamespace())
			continue
		}

		sourceSecretNamespacedName := secretReplicator.GetSourceSecretNamespacedName()

		//Ensure we are listening the source secret events
		if !r.sources.Exists(sourceSecretNamespacedName) {
			r.sources.Set(sourceSecretNamespacedName, secretReplicator.GetName())
		}

		//Get the source Secret
		sourceSecret := &corev1.Secret{}
		if err := r.client.Get(context.TODO(), sourceSecretNamespacedName, sourceSecret); err != nil {
			if errors.IsNotFound(err) {
				reqLogger.Info("Secret to replicate does not exists",
					"Name", secretReplicator.GetName(), "Namespace", secretReplicator.GetSourceNamespace())
				r.recorder.Eventf(&secretReplicator, corev1.EventTypeWarning, "NotFound",
					"Secret %s/%s does not exists", secretReplicator.GetSourceNamespace(), secretReplicator.GetSourceName())
				continue
			}
			return reconcile.Result{}, err
		}

		if !isReplicationAllowed(&secretReplicator) {
			reqLogger.Info("Secret to replicate does not allow to be replicated",
				"Name", secretReplicator.GetName(), "Namespace", secretReplicator.GetSourceNamespace())
			r.recorder.Eventf(&secretReplicator, corev1.EventTypeWarning, "ReplicationForbidden",
				"Secret %s/%s does not allow to be replicated", secretReplicator.GetSourceNamespace(), secretReplicator.GetSourceName())
			continue
		}

		//Delete Old Secrets if ReplicantsName has change
		if secretReplicator.GetReplicantsName() != secretReplicator.GetStatusReplicantsName() {
			if secretReplicator.GetStatusReplicantsName() != "" {
				for _, namespace := range namespaceList.Items {
					namespacedName := types.NamespacedName{Name: secretReplicator.GetStatusReplicantsName(), Namespace: namespace.GetName()}
					if err := r.deleteSecret(namespacedName); err != nil {
						log.Error(err, "Failed to delete the previous Secret", "Namespace", namespacedName.Namespace, "Name", namespacedName.Name)
						return reconcile.Result{}, err
					}
					r.sources.Delete(namespacedName)
				}
			}

			oldReplicantsName := secretReplicator.GetStatusReplicantsName()
			secretReplicator.SetStatusReplicantsName(secretReplicator.GetReplicantsName())

			//Update SecretReplicator status
			if err := r.client.Status().Update(context.TODO(), &secretReplicator); err != nil {
				reqLogger.Info("Failed to update SecretReplicator's status",
					"Name", secretReplicator.GetName(), "Namespace", secretReplicator.GetSourceNamespace())
				return reconcile.Result{}, err
			}

			if oldReplicantsName != "" {
				r.recorder.Eventf(&secretReplicator, corev1.EventTypeNormal, "Changed",
					"ReplicantsName has changed from \"%s\" to \"%s\"", oldReplicantsName, secretReplicator.GetStatusReplicantsName())
			}
		}

		selector, err := secretReplicator.GetNamespacesSelector()
		if err != nil {
			log.Error(err, "")
			return reconcile.Result{}, err
		}

		changes := false
		for _, namespace := range namespaceList.Items {
			//Check if the namespace match the SecretReplicator's NamespaceSelector
			if selector.Empty() || selector.Matches(labels.Set(namespace.GetLabels())) {
				//Skip if the target secret is the same as the source secret in this namespace
				if namespace.GetName() == sourceSecret.GetNamespace() && secretReplicator.GetStatusReplicantsName() == sourceSecret.GetName() {
					continue
				}

				changed, err := r.replicateSecret(&secretReplicator, sourceSecret, namespace.GetName())
				if err != nil {
					reqLogger.Error(err, "Failed to replicate Secret.", "Name", secretReplicator.GetReplicantsName(), "Namespace", namespace.GetName())
					return reconcile.Result{}, err
				}
				if changed {
					changes = true
				}
			} else {
				//Ensure that there is no replicant in the namespace
				namespacedName := types.NamespacedName{Name: secretReplicator.GetReplicantsName(), Namespace: namespace.GetName()}
				if err := r.deleteSecret(namespacedName); err != nil {
					log.Error(err, "Failed to delete the Secret", "Namespace", namespacedName.Namespace, "Name", namespacedName.Name)
					return reconcile.Result{}, err
				}
			}
		}

		//Record an event
		if changes {
			r.recorder.Event(&secretReplicator, corev1.EventTypeNormal, "Synced", "All replicants (Secrets) are synced")
		}
	}
	return reconcile.Result{}, nil
}

func (r *ReconcilesecretReplicator) replicateSecret(secretReplicator *ritmxv1.SecretReplicator, sourceSecret *corev1.Secret, targetNamespace string) (bool, error) {
	secret := &corev1.Secret{}
	secret.SetName(secretReplicator.GetStatusReplicantsName())
	secret.SetNamespace(targetNamespace)
	result, err := controllerutil.CreateOrUpdate(context.TODO(), r.client, secret, func() error {
		if err := replicateSecretInto(sourceSecret, secret); err != nil {
			return err
		}
		if err := controllerutil.SetControllerReference(secretReplicator, secret, r.scheme); err != nil {
			return err
		}
		return nil
	})

	switch result {
	case controllerutil.OperationResultNone:
	case controllerutil.OperationResultCreated:
		log.Info("Secret created", "Name", secret.GetName(), "Namespace", secret.GetNamespace())
		r.recorder.Eventf(secret, corev1.EventTypeNormal, "Created", "Secret created %s/%s", secret.GetNamespace(), secret.GetName())
	case controllerutil.OperationResultUpdated:
		log.Info("Secret updated", "Name", secret.GetName(), "Namespace", secret.GetNamespace())
		r.recorder.Eventf(secret, corev1.EventTypeNormal, "Updated", "Secret updated %s/%s", secret.GetNamespace(), secret.GetName())
	}
	if err != nil {
		return false, err
	}
	return result != controllerutil.OperationResultNone, nil
}

func (r *ReconcilesecretReplicator) deleteSecret(namespacedName types.NamespacedName) error {
	oldSecret := &corev1.Secret{}
	if err := r.client.Get(context.TODO(), namespacedName, oldSecret); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		log.Error(err, "Failed to get the previous Secret", "Namespace", namespacedName.Namespace, "Name", namespacedName.Name)
		return err
	}
	//Verify that we are this Secret's controller
	controller := metav1.GetControllerOf(oldSecret)
	if controller != nil {
		controllerGV, err := schema.ParseGroupVersion(controller.APIVersion)
		if err != nil {
			return err
		}

		if controller.Kind == r.crdKind && controllerGV.Group == ritmxv1.SchemeGroupVersion.Group {
			if err := r.client.Delete(context.TODO(), oldSecret); err != nil {
				if !errors.IsGone(err) {
					return err
				}
			} else {
				log.Info("Secret deleted", "Name", oldSecret.GetName(), "Namespace", oldSecret.GetNamespace())
			}
		}
	}
	return nil
}

func (r *ReconcilesecretReplicator) listSecretReplicators(name string) (*ritmxv1.SecretReplicatorList, error) {
	secretReplicatorList := &ritmxv1.SecretReplicatorList{}
	if name == "" {
		err := r.client.List(context.TODO(), secretReplicatorList)
		if err != nil {
			return nil, err
		}
	} else {
		secretReplicator := &ritmxv1.SecretReplicator{}
		err := r.client.Get(context.TODO(), types.NamespacedName{Name: name}, secretReplicator)
		if err != nil {
			if errors.IsNotFound(err) {
				secretReplicatorList.Items = []ritmxv1.SecretReplicator{}
				return secretReplicatorList, nil
			}
			return nil, err
		}
		secretReplicatorList.Items = []ritmxv1.SecretReplicator{*secretReplicator}
	}
	return secretReplicatorList, nil
}

func (r *ReconcilesecretReplicator) listNamespaces(name string) (*corev1.NamespaceList, error) {
	namespaceList := &corev1.NamespaceList{}
	if name == "" {
		err := r.client.List(context.TODO(), namespaceList)
		if err != nil {
			return nil, err
		}
	} else {
		namespace := &corev1.Namespace{}
		err := r.client.Get(context.TODO(), types.NamespacedName{Name: name}, namespace)
		if err != nil {
			if errors.IsNotFound(err) {
				namespaceList.Items = []corev1.Namespace{}
				return namespaceList, nil
			}
			return nil, err
		}
		namespaceList.Items = []corev1.Namespace{*namespace}
	}
	return namespaceList, nil
}

func (r *ReconcilesecretReplicator) validateSecretReplicator(secretReplicator *ritmxv1.SecretReplicator) bool {
	isValid := true
	if errors := validation.IsQualifiedName(secretReplicator.GetSourceName()); errors != nil {
		r.recorder.Eventf(secretReplicator, corev1.EventTypeWarning, "Invalid", "Name is invalid; %v", errors)
		isValid = false
	}
	if errors := validation.IsQualifiedName(secretReplicator.GetSourceNamespace()); errors != nil {
		r.recorder.Eventf(secretReplicator, corev1.EventTypeWarning, "Invalid", "Namespace is invalid: %v", errors)
		isValid = false
	}
	if errors := validation.IsQualifiedName(secretReplicator.GetReplicantsName()); errors != nil {
		r.recorder.Eventf(secretReplicator, corev1.EventTypeWarning, "Invalid", "ReplicantsName is invalid: %v", errors)
		isValid = false
	}
	return isValid
}

var isReplicationAllowed = func() func(*ritmxv1.SecretReplicator) bool {
	enabled, _ := regexp.MatchString("^(y|Y|yes|Yes|YES|true|True|TRUE|on|On|ON|1)$", os.Getenv("SR_CHECK_PERMISSION"))
	if enabled {
		return func(secretReplicator *ritmxv1.SecretReplicator) bool {
			value, ok := secretReplicator.GetAnnotations()[annotationAllowReplicate]
			if ok {
				match, err := regexp.MatchString("^(y|Y|yes|Yes|YES|true|True|TRUE|on|On|ON)$", value)
				if err == nil && match {
					return true
				}
			}
			return false
		}
	}
	return func(*ritmxv1.SecretReplicator) bool {
		return true
	}
}()
