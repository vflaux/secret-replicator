/*
Copyright 2019 The secret-replicator Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package secretreplicator

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type secretWatchFilter struct {
	//mutex sync.RWMutex
	names map[types.NamespacedName]string
}

func newSecretWatchFilter() *secretWatchFilter {
	return &secretWatchFilter{
		names: make(map[types.NamespacedName]string),
	}
}

func (s *secretWatchFilter) Get(key types.NamespacedName) (string, bool) {
	//s.mutex.RLock()
	name, ok := s.names[key]
	//s.mutex.RUnlock()
	return name, ok
}

func (s *secretWatchFilter) Exists(key types.NamespacedName) bool {
	//s.mutex.RLock()
	_, ok := s.names[key]
	//s.mutex.RUnlock()
	return ok
}

func (s *secretWatchFilter) Set(key types.NamespacedName, value string) {
	//s.mutex.Lock()
	s.names[key] = value
	//s.mutex.Unlock()
}

func (s *secretWatchFilter) Delete(key types.NamespacedName) {
	//s.mutex.Lock()
	delete(s.names, key)
	//s.mutex.Unlock()
}

func (s *secretWatchFilter) DeleteByValue(name string) {
	//s.mutex.Lock()
	for key, value := range s.names {
		if value == name {
			delete(s.names, key)
		}
	}
	//s.mutex.Unlock()
}

func (s *secretWatchFilter) Filter(o metav1.Object) bool {
	namespacedName := types.NamespacedName{Name: o.GetName(), Namespace: o.GetNamespace()}
	return s.Exists(namespacedName)
}

func replicateSecretInto(in *corev1.Secret, out *corev1.Secret) error {

	if in.ObjectMeta.Labels != nil {
		in, out := &in.ObjectMeta.Labels, &out.ObjectMeta.Labels
		*out = make(map[string]string, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	} else {
		out.ObjectMeta.Labels = nil
	}

	if in.ObjectMeta.Annotations != nil {
		in, out := &in.ObjectMeta.Annotations, &out.ObjectMeta.Annotations
		*out = make(map[string]string, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	} else {
		out.ObjectMeta.Annotations = nil
	}

	out.Type = in.Type

	if in.Data != nil {
		in, out := &in.Data, &out.Data
		*out = make(map[string][]byte, len(*in))
		for key, val := range *in {
			var outVal []byte
			if val == nil {
				(*out)[key] = nil
			} else {
				in, out := &val, &outVal
				*out = make([]byte, len(*in))
				copy(*out, *in)
			}
			(*out)[key] = outVal
		}
	} else {
		out.Data = nil
	}
	if in.StringData != nil {
		in, out := &in.StringData, &out.StringData
		*out = make(map[string]string, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	} else {
		out.StringData = nil
	}

	return nil
}
