.PHONY: generate
generate:
	operator-sdk generate k8s
	operator-sdk generate openapi
